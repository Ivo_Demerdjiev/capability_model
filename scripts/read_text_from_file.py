#!/usr/bin/env python
# coding: utf-8

import os
import sys
import json
import pandas as pd
import numpy as np
import nltk
from nltk.corpus import stopwords
from nltk import word_tokenize
from nltk.stem.snowball import SnowballStemmer
import re
import logging
import threading
import gensim
import docx

from io import StringIO

from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfdocument import PDFDocument
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.pdfpage import PDFPage
from pdfminer.pdfparser import PDFParser

def read_text_from_pdf(pdf_file):
    '''
        This function takes a .pdf file and returns a string, containing all the text in the file. 
        It works only for searchable, but not for scanned .pdf files.
    '''

    output_string = StringIO()
    with open(pdf_file, 'rb') as in_file:
        parser = PDFParser(in_file)
        doc = PDFDocument(parser)
        rsrcmgr = PDFResourceManager()
        device = TextConverter(rsrcmgr, output_string, codec='utf-8', laparams=LAParams())
        interpreter = PDFPageInterpreter(rsrcmgr, device)
        for page in PDFPage.create_pages(doc):
            interpreter.process_page(page)

    return(output_string.getvalue())

def read_text_from_docx(docx_file):
    '''
        This function takes a .docx file and returns a string, containing all the text in the file.    
    '''
    
    doc = docx.Document(docx_file)
    full_text = []
    for paragraph in doc.paragraphs:
        txt = paragraph.text.encode('utf-8').decode('utf-8')
        full_text.append(txt)
    return '\n'.join(full_text)


def check_file_extension(file):
    '''
        Check the file extension due to the fact the logic behind reading the text is different in terms of 
        .pdf and .docx.
    '''
    pdf_suffix = 'pdf'
    
    text = read_text_from_pdf(file) if file.endswith(pdf_suffix) else read_text_from_docx(file)
    
    return text


def remove_unneeded_chars_file(text):
    '''
        Remove every character that is not a letter. Also, replace multiple consecutive spaces with one space.
    '''
    
    reg_expression = "[^A-Za-z]+"
    
    new_text = re.sub(reg_expression, " ", text)
    
    return new_text


def remove_stop_words_file(text): 
    
    stop_words = stopwords.words('english')
    word_tokens = word_tokenize(text)  
  
    filtered_sentence = [word for word in word_tokens if not word in stop_words] 
    
    return (" ").join(filtered_sentence)


def remove_short_words_file(text):
    '''  
        Remove words with length less or equal to 2.
    '''
    new_text = " ".join(filter(lambda item: len(item) > 2, text.split(" ")))
        
    return new_text  



def stem_file(text, stemmer):
    '''
       Stem a single file. 
    '''  
    stemmer = SnowballStemmer("english")
    stemmed_text = " ".join(
        map(lambda item: stemmer.stem(item), text.split(" ")))
    
    return stemmed_text


def clean_text_file(file):
    '''
        Combine all the steps with the purpose of deriving clean text.
    '''
    stemmer = SnowballStemmer("english")
    
    text = check_file_extension(file)
    text_clean = remove_unneeded_chars_file(text)
    text_clean = remove_stop_words_file(text_clean)
    text_clean = remove_short_words_file(text_clean)
    text_clean = stem_file(text_clean, stemmer)
    
    return [text_clean]


