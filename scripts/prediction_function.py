#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os
import sys
import pandas as pd
import numpy as np

from tensorflow import keras
from keras.models import load_model
from keras.preprocessing.text import Tokenizer, tokenizer_from_json
from keras.preprocessing.sequence import pad_sequences


# In[2]:


#os.getcwd()


# In[2]:


sys.path.append('../scripts/')
from read_text_from_file import *


# In[21]:


PATH_BEST_MODELS = "../model/best_models/"
PATH_TOKENIZER = "../tokenizer/"

MAX_LEN = 300

test_file = "../data/pdf_files/antitrust_manproc_11_2019_en.pdf"


# In[22]:


f = open(PATH_TOKENIZER + 'tokenizer.json', "r")
d = json.load(f)
tokenizer = tokenizer_from_json(d)


# In[31]:


capability_model = load_model(PATH_BEST_MODELS + 'capability_model_extended.hdf5')


# In[32]:


#The sequence of labels is not subject to change!!!
labels = ['Administration coherence', 'Administrative Law', 'Business Continuity Planning', 'Business Intelligence',
       'Data Governance', 'Data Integration & Interoperability','Data Quality', 'Data Security', 
       'Data Storage & Operations', 'Data management', 'Digital Services ', 'Digital Strategy','Digital innovation', 
       'Digital policy', 'Digital technology','EU policy implementation', 'EU policymaking',
       'Education programme implementation','Education, youth, culture and sport promotion',
       'Employee Information Management', 'HR mobile strategy','Health and safety standards', 
       'Human Resource Governance','ICT Service delivery', 'Info Economy','Inter-institutional information exchange', 
       'Legal Advice','Legal support', 'Legislation and standards definition','Legislation and standards enforcement',
       'Policy definition coordination', 'Reference & Master Data','Security control', 'Strategy and Planning',
       'Tools, platforms and services', 'Trade', 'Trade Development','Trade Missions', 'Trade regulation instruments',
       'Vocational training support', 'Workforce Capability Development','Workforce mobility']


# In[33]:


def get_feature_vectors(file):
    '''
        To transform the text from a file into feature_vectors suitable for prediction purposes.
        Tokenizer must be available.
    '''   
    list_text = clean_text_file(file)
    sequences = tokenizer.texts_to_sequences(list_text)
    return pad_sequences(sequences, maxlen=MAX_LEN)


# In[34]:


def capability_model_prediction(file, capability_model, threshold=0.5):
    '''
        Returns a dictionary in the following form: {'capability': confidence}, sorted in a descending order. Also, it 
        takes into consideration predictions with confidence >= 0.5.
    '''

    feature_vector = get_feature_vectors(file)
    prediction = capability_model.predict(feature_vector)
    
    conf = [(labels[i], prob) for i, prob in enumerate(prediction.flatten().tolist()) if prob>=threshold]
    
    return dict(sorted(conf, key=lambda kv: kv[1], reverse=True))


# In[35]:


#capabilities = capability_model_prediction(test_file, capability_model, threshold=0.5)


# In[37]:


#capabilities

