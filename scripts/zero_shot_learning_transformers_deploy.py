#!/usr/bin/env python
# coding: utf-8

# In[87]:


get_ipython().system('pip install git+https://github.com/huggingface/transformers')


# In[88]:


get_ipython().system('pip install -U nlp')


# In[79]:


import pandas as pd
import numpy as np
import os
import sys
import io
#import warnings

import tensorflow as tf

from tensorflow import keras
from keras.models import Model

from transformers import (AutoTokenizer, TFAutoModel, pipeline, TFAutoModelForSequenceClassification)


# In[2]:


sys.path.append('../scripts/')
from read_text_from_file import *


# In[3]:


datasets = "/datasets/"
path_datasets = "..{0}".format(datasets)

PATH_MODEL_SUMMARIZER = "../model/summarizer/"
PATH_MODEL_CLASSIFIER = "../model/classifier/"


# In[90]:


df_capability = pd.read_csv(path_datasets + 'business_capabilities.csv')
df_tech_capability = pd.read_csv(path_datasets + 'technical_capabilities.csv')
df_car_manufacturing = pd.read_csv(path_datasets + 'car_manufacturing_capabilities.csv')


# In[5]:


business_capabilities = list(set(df_capability['Capability']))
technical_capabilities = list(set(df_tech_capability['Name']))
car_manufactoring_capabilities = list(set(df_car_manufacturing['Name']))


#test_file = "../data/docx_files/DSMP input OIB_mobile devices.docx"

text = remove_unneeded_chars_file(check_file_extension(test_file))


# ### Summarizer


summarizer = pipeline("summarization", model=PATH_MODEL_SUMMARIZER, tokenizer=PATH_MODEL_SUMMARIZER, device=0, framework='tf')# device=0 to utilize GPU')

def summarization(text, summarizer):
    '''
        It returns the text extracted from a document in a summarized form.
    '''
    summary_dict = summarizer(text, max_length=400, min_length=100, do_sample=False)[0]
    
    return [value for key, value in summary_dict.items()]


# ### Classifier

classifier = pipeline("zero-shot-classification", model=PATH_MODEL_CLASSIFIER, tokenizer=PATH_MODEL_CLASSIFIER, device=0, framework='tf') # device=0 to utilize GPU

# ### Predictions

def zero_shot_predictions(text, capabilities_list, threshold=0.5):
    '''
        It returns capabilities relevant for the respective document. A list of both business and technical capablities can be
        used for prediction purposes.
    '''
    summary_text = summarization(text, summarizer)
    
    capabilities_predictions = classifier(summary_text, capabilities_list, multi_label=True)
    capabilities_confidences = [(capabilities_predictions['labels'][i], score) 
                                for i, score in enumerate(capabilities_predictions['scores']) if score > threshold]
    
    return dict(sorted(capabilities_confidences, key=lambda kv: kv[1], reverse=True))


# In[84]:


#zero_shot_predictions(text, business_capabilities, threshold=0.5)


# In[86]:


#zero_shot_predictions(text, technical_capabilities, threshold=0.5)


# In[ ]:




